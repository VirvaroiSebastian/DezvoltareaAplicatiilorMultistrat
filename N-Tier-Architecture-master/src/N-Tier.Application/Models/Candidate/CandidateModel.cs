﻿namespace N_Tier.Application.Models.Candidate
{
    public class CandidateModel
    {
        public string InterviewId { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string BirthDay { get; set; }

        public string CNP { get; set; }

        public string PhoneNumber { get; set; }

        public string Email { get; set; }
    }
}
