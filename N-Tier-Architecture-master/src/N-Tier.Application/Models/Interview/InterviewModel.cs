﻿using System;

namespace N_Tier.Application.Models.Interview
{
    public class InterviewModel
    {
        public string Title { get; set; }

        public string Department { get; set; }

        public int NumberOfPositions { get; set; }
    }
}
