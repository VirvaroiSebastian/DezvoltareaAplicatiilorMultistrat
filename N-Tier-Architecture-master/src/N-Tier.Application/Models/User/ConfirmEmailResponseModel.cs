﻿namespace N_Tier.Application.Models.User
{
    public class ConfirmEmailResponseModel
    {
        public bool Confirmed { get; set; }
    }
}
