﻿using AutoMapper;
using N_Tier.Application.Models.Candidate;
using N_Tier.Core.Entities;

namespace N_Tier.Application.MappingProfiles
{

    public class CandidateProfile : Profile
    {
        public CandidateProfile()
        {
            CreateMap<Candidate, CandidateModel>();
        }
        
    }
}
