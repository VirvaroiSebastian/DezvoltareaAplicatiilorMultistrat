﻿using N_Tier.Application.Models.Interview;
using System;
using System.Threading.Tasks;

namespace N_Tier.Application.Services
{
    public interface IInterviewService
    {
        Task<Guid> AddInterviewAsync(InterviewModel interviewModel);
        Task<Guid> DeleteAsync(Guid id);
        //Task<IEnumerable<InterviewModel>> GetAllAsync();

        Task<Guid> UpdateAsync(Guid id, InterviewModel updateInterviewModel);
    }
}
