﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using N_Tier.Application.Models.Test;

namespace N_Tier.Application.Services
{
    public interface ITestService
    {
        Task<Guid> AddTestAsync(TestModel testModel);
        Task<Guid> DeleteTestAsynct(Guid id);
        Task<IEnumerable<TestModel>> GetAllByListIdAsync(Guid id);

        Task<Guid> UpdateAsync(Guid id, TestModel updateTestModel);
    }
}
