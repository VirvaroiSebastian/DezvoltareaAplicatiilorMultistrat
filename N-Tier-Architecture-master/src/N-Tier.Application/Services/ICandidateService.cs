﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using N_Tier.Application.Models.Candidate;

namespace N_Tier.Application
{
    public interface ICandidateService
    {
        Task<Guid> AddCandidateAsync(CandidateModel candidateModel);

        Task<Guid> DeleteAsync(Guid id);

        Task<IEnumerable<CandidateModel>> GetAllAsync();

        Task<IEnumerable<CandidateModel>> GetAllByListIdAsync(Guid id);

        Task<Guid> UpdateAsync(Guid id, CandidateModel updateCandidateModel);
    } 
}
