﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using N_Tier.Application.Models.Question;

namespace N_Tier.Application.Services
{
    public interface IQuestionService
    {
        Task<Guid> CreateQuestionAsync(QuestionModel questionModel);
        Task<Guid> DeleteQuestionAsync(Guid id);
        Task<IEnumerable<QuestionModel>> GetAllByListIdAsync(Guid id);

        Task<Guid> UpdateAsync(Guid id, QuestionModel updateQuestionModel);
    }
}
