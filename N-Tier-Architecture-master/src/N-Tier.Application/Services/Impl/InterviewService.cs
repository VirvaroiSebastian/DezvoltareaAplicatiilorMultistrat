﻿using AutoMapper;
using N_Tier.Application.Exceptions;
using N_Tier.Application.Models.Interview;
using N_Tier.Core.Entities;
using N_Tier.DataAccess.Repositories;
using N_Tier.Shared.Services;
using System;
using System.Threading.Tasks;

namespace N_Tier.Application.Services.Impl
{
    public class InterviewService : IInterviewService
    {
        private readonly IInterviewRepository _interviewRepository;
        private readonly IClaimService _claimService;
        private readonly IMapper _mapper;

        public InterviewService(IInterviewRepository interviewRepository)
        {
            _interviewRepository = interviewRepository;
        }

        public async Task<Guid> AddInterviewAsync(InterviewModel interviewModel)
        {
            var interview = new Interview()
            {
                Title =  interviewModel.Title,
                Department = interviewModel.Department,
                NumberOfPositions = interviewModel.NumberOfPositions
            };

            var addedInterview = await _interviewRepository.AddAsync(interview);

            return addedInterview.Id;
        }

        public async Task<Guid> DeleteAsync(Guid id)
        {
            var interview = await _interviewRepository.GetFirstAsync(c => c.Id == id);
            if (interview == null)
                throw new NotFoundException("The interview does not exist anymore");
            return (await _interviewRepository.DeleteAsync(interview)).Id;
        }

        //public async Task<IEnumerable<InterviewModel>> GetAllAsync()
        //{
        //    var title = _claimService.GetUserId();

        //    var interview = await _interviewRepository.GetAsync(i => i.Title = title);

        //    return _mapper.Map<IEnumerable<InterviewModel>>(interview);
        //}


        public async Task<Guid> UpdateAsync(Guid id, InterviewModel updateInterviewModel)
        {
            var interview = await _interviewRepository.GetFirstAsync(c => c.Id == id);

            if (interview == null)
                throw new NotFoundException("Candidate does not exist anymore");

            interview.Title = updateInterviewModel.Title;
            interview.Department = updateInterviewModel.Department;
            interview.NumberOfPositions = updateInterviewModel.NumberOfPositions;

            return (await _interviewRepository.UpdateAsync(interview)).Id;
        }
    }
}
