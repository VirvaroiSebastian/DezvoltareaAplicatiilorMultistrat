﻿using AutoMapper;
using N_Tier.Application.Exceptions;
using N_Tier.Application.Models.Candidate;
using N_Tier.Core.Entities;
using N_Tier.DataAccess.Repositories;
using N_Tier.Shared.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace N_Tier.Application.Services.Impl
{
    public class CandidateService : ICandidateService
    {
        private readonly ICandidateRepository _candidateRepository;
        private readonly IMapper _mapper;
        private readonly IInterviewRepository _interviewRepository;
        private readonly IClaimService _claimService;
        private readonly CandidateModel _candidateModel;

        public CandidateService(ICandidateRepository candidateRepository, IMapper mapper, 
            IInterviewRepository interviewRepository )
        {
            _candidateRepository = candidateRepository;
            _mapper = mapper;
            _interviewRepository = interviewRepository;
        }

        public async Task<Guid> AddCandidateAsync(CandidateModel candidateModel)
        {
            //aici fac o conexiune cu alta tabela, preconditie 
            var interview = await _interviewRepository.GetFirstAsync(i => i.Id == Guid.Parse(candidateModel.InterviewId));
            var candidate = new Candidate()
            {
                Interview = interview,
                Name = candidateModel.Name,
                Surname = candidateModel.Surname,
                BirthDay = candidateModel.BirthDay,
                CNP = candidateModel.CNP,
                PhoneNumber = candidateModel.PhoneNumber,
                Email = candidateModel.Email
            };

            var addedCandidate = await _candidateRepository.AddAsync(candidate);

            return addedCandidate.Id;
        }

        public async Task<Guid> DeleteAsync(Guid id)
        {
            var candidate = await _candidateRepository.GetFirstAsync(c => c.Id == id);
            if (candidate == null)
                throw new NotFoundException("The candidate does not exist anymore");
            return (await _candidateRepository.DeleteAsync(candidate)).Id;
        }

        public async Task<IEnumerable<CandidateModel>> GetAllAsync()
        {
            var currentUserId = _claimService.GetUserId();
            Console.WriteLine(currentUserId);

            var candidate = await _candidateRepository.GetAsync(tl => tl.Id != null);

            return _mapper.Map<IEnumerable<CandidateModel>>(candidate);

            //return Enumerable.Range(1, 10).Select(index => new CandidateModel()
            //{
            //    Name = _candidateModel.Name,
            //    Surname = _candidateModel.Surname,
            //    BirthDay = _candidateModel.BirthDay,
            //    CNP = _candidateModel.CNP,
            //    PhoneNumber = _candidateModel.PhoneNumber,
            //    Email = _candidateModel.Email,
            //    //InterviewId = candidateModel.InterviewId
            //});
        }

        public async Task<IEnumerable<CandidateModel>> GetAllByListIdAsync(Guid id)
        {
            var candidates = await _candidateRepository.GetAsync(c => c.Interview.Id == id);
            return _mapper.Map<IEnumerable<CandidateModel>>(candidates);
        }

        public async Task<Guid> UpdateAsync(Guid id, CandidateModel updateCandidateModel)
        {
            var candidate = await _candidateRepository.GetFirstAsync(c => c.Id == id);

            if (candidate == null)
                throw new NotFoundException("Candidate does not exist anymore");

            candidate.Name = updateCandidateModel.Name;
            candidate.Surname = updateCandidateModel.Surname;
            candidate.BirthDay = updateCandidateModel.BirthDay;
            candidate.CNP = updateCandidateModel.CNP;
            candidate.Email = updateCandidateModel.Email;
            candidate.PhoneNumber = updateCandidateModel.PhoneNumber;


            return (await _candidateRepository.UpdateAsync(candidate)).Id;
        }

    }
}
