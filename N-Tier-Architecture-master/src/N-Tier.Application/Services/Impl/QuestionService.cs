﻿using AutoMapper;
using N_Tier.Application.Exceptions;
using N_Tier.Application.Models.Question;
using N_Tier.Core.Entities;
using N_Tier.DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace N_Tier.Application.Services.Impl
{
    public class QuestionService : IQuestionService
    {
        private readonly IQuestionRepository _questionRepository;
       // private readonly IMapper _mapper;

        public QuestionService(IQuestionRepository questionRepository, IMapper mapper)
        {
            _questionRepository = questionRepository;
           // _mapper = mapper;
        }

        public async Task<Guid> CreateQuestionAsync(QuestionModel questionModel)
        {
            var question = new Question()
            {
                Statement = questionModel.Statement
            };

            var addedQuestion = await _questionRepository.AddAsync(question);

            return addedQuestion.Id;
        }

        public async Task<Guid> DeleteQuestionAsync(Guid id)
        {
            var candidate = await _questionRepository.GetFirstAsync(c => c.Id == id);
            if (candidate == null)
                throw new NotFoundException("The question does not exist anymore");
            return (await _questionRepository.DeleteAsync(candidate)).Id;
        }

        public Task<IEnumerable<QuestionModel>> GetAllByListIdAsync(Guid id)
        {
            throw new NotImplementedException();
        }

        //public async Task<IEnumerable<QuestionModel>> GetAllByListIdAsync(Guid id)
        //{
        //    var questions = await _questionRepository.GetAsync(c => c..Id == id);
        //    return _mapper.Map<IEnumerable<QuestionModel>>(questions);
        //}

        public async Task<Guid> UpdateAsync(Guid id, QuestionModel updateQuestionModel)
        {
            var question = await _questionRepository.GetFirstAsync(c => c.Id == id);

            if (question == null)
                throw new NotFoundException("Question does not exist anymore");

            question.Statement = updateQuestionModel.Statement;

            return (await _questionRepository.UpdateAsync(question)).Id;
        }
    }
}
