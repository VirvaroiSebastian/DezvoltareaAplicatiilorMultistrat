﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using N_Tier.Application;
using N_Tier.Application.Models;
using N_Tier.Application.Models.Candidate;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace N_Tier.API.Controllers
{
    public class CandidatesController : ApiController
    {
        
        private readonly ICandidateService _candidateService;

        public CandidatesController(ICandidateService candidateService)
        {
            _candidateService = candidateService;
        }

        [HttpPost]
        public async Task<IActionResult> AddCandidateAsync(CandidateModel candidateModel)
        {
            var result = await _candidateService.AddCandidateAsync(candidateModel);

            return Ok(result);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateAsync(Guid id, CandidateModel updateCandidateModel)
        {
            return Ok(ApiResult<Guid>.Success200(await _candidateService.UpdateAsync(id, updateCandidateModel)));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            return Ok(ApiResult<Guid>.Success200(await _candidateService.DeleteAsync(id)));
        }

        [HttpGet("/api/interviews/id/candidates")]
        public async Task<IActionResult> GetAllTodoItemsAsync(Guid id)
        {
            return Ok(ApiResult<IEnumerable<CandidateModel>>.Success200(await _candidateService.GetAllByListIdAsync(id)));
        }

        [HttpGet]
        public async Task<IActionResult> GetAllAsync()
        {
            //return Ok(ApiResult<IEnumerable<CandidateModel>>.Success200(
            //await _candidateService.GetAllAsync()));
            return Ok(await _candidateService.GetAllAsync());
        }
    }
}
