﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using N_Tier.Application.Models;
using N_Tier.Application.Models.Interview;
using N_Tier.Application.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace N_Tier.API.Controllers
{
    public class InterviewsController : ApiController
    {
        private readonly IInterviewService _interviewService;

        public InterviewsController(IInterviewService interviewService)
        {
            _interviewService = interviewService;
        }

        [HttpPost]
        public async Task<IActionResult> AddInterviewAsync(InterviewModel interviewModel)
        {
            var result = await _interviewService.AddInterviewAsync(interviewModel);

            return Ok(result);
        }
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateAsync(Guid id, InterviewModel updateInterviewModel)
        {
            return Ok(ApiResult<Guid>.Success200(await _interviewService.UpdateAsync(id, updateInterviewModel)));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            return Ok(ApiResult<Guid>.Success200(await _interviewService.DeleteAsync(id)));
        }

        //[HttpGet]
        //public async Task<IActionResult> GetAll()
        //{
        //    return Ok(ApiResult<IEnumerable<InterviewModel>>.Success200(await _interviewService.GetAllAsync()));
        //}
    }
}
