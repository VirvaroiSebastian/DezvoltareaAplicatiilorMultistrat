﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using N_Tier.Application.Models;
using N_Tier.Application.Models.Question;
using N_Tier.Application.Services;
using System;
using System.Threading.Tasks;

namespace N_Tier.API.Controllers
{
    public class QuestionsController : ApiController
    {
        private readonly IQuestionService _questionService;

        public QuestionsController(IQuestionService questionService)
        {
            _questionService = questionService;
        }

        [HttpPost]
        public async Task<IActionResult> AddQuestionAsync(QuestionModel questionModel)
        {
            var result = await _questionService.CreateQuestionAsync(questionModel);

            return Ok(result);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateAsync(Guid id, QuestionModel updateQuestionModel)
        {
            return Ok(ApiResult<Guid>.Success200(await _questionService.UpdateAsync(id, updateQuestionModel)));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            return Ok(ApiResult<Guid>.Success200(await _questionService.DeleteQuestionAsync(id)));
        }

        //[HttpGet("/api/interviews/id/candidates")]
        //public async Task<IActionResult> GetAllTodoItemsAsync(Guid id)
        //{
        //    return Ok(ApiResult<IEnumerable<CandidateModel>>.Success200(await _questionService.GetAllByListIdAsync(id)));
        //}
    }
}
