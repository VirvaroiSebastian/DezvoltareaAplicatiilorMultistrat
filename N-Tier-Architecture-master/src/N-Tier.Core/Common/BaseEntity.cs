﻿namespace N_Tier.Core.Common
{ 
    using System;
    public abstract class BaseEntity
    {
        public Guid Id { get; set; }
    }
}
