﻿namespace N_Tier.Core.Entities
{
    using Common;
    using System;
    using System.Collections.Generic;

    public class Interview : BaseEntity
    {
        public string Department { get; set; }

        public string Title { get; set; }

        public int NumberOfPositions { get; set; }

        public List<Candidate> Candidates { get; set; }

        public Test Test { get; set; }

        public DateTime InterviewDate { get ; set ; }
    }
}
