﻿using System;
using N_Tier.Core.Common;
using System.Collections.Generic;

namespace N_Tier.Core.Entities
{
    public class Test : BaseEntity
    {
        public Guid InterviewId { get; set; }

        public Interview Interview { get; set; }

        public string Position { get; set; }

        public List<Question> Questions { get; set; }

        public int TimeLimit { get; set; }

    }
}
