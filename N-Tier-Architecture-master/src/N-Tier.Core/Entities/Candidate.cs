﻿namespace N_Tier.Core.Entities
{
    using Common;
    public class Candidate : BaseEntity
    {
        public string Name { get; set; }

        public string Surname { get; set;}

        public string BirthDay { get; set; }

        public string CNP { get; set; }

        public string PhoneNumber { get; set;}

        public string Email { get; set; }

        public Interview Interview { get; set; }
    }
}
