﻿using N_Tier.Core.Common;

namespace N_Tier.Core.Entities
{
    public  class Question : BaseEntity
    {
        public string Statement { get; set; }
        public Test Test { get; set; }
    }
}
