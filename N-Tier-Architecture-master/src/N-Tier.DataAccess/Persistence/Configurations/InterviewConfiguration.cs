﻿namespace N_Tier.DataAccess.Persistence.Configurations
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using Core.Entities;

    public class InterviewConfiguration : IEntityTypeConfiguration<Interview>
    {
        public void Configure(EntityTypeBuilder<Interview> builder)
        {
            builder.HasMany(i => i.Candidates)
                    .WithOne(c => c.Interview)
                    .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(i => i.Test)
                .WithOne(t => t.Interview)
                .HasForeignKey<Test>(t => t.InterviewId);
        }
    }
}
