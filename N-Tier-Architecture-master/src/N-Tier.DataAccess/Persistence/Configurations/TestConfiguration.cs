﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using N_Tier.Core.Entities;
using System;

namespace N_Tier.DataAccess.Persistence.Configurations
{
    public class TestConfiguration : IEntityTypeConfiguration<Test>
    {
        public void Configure(EntityTypeBuilder<Test> builder)
        {
            builder.HasMany(t => t.Questions)
                .WithOne(q => q.Test)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
