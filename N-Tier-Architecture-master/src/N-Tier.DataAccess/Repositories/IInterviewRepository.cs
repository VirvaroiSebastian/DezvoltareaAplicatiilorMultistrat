﻿using N_Tier.Core.Entities;

namespace N_Tier.DataAccess.Repositories
{
    public interface IInterviewRepository : IBaseRepository<Interview>
    { }
}
