// <auto-generated/>
#pragma warning disable 1591
#pragma warning disable 0414
#pragma warning disable 0649
#pragma warning disable 0169

namespace FeDam.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 2 "C:\Users\Sebastian\source\repos\FeDam\FeDam\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\Sebastian\source\repos\FeDam\FeDam\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\Sebastian\source\repos\FeDam\FeDam\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\Sebastian\source\repos\FeDam\FeDam\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\Sebastian\source\repos\FeDam\FeDam\_Imports.razor"
using Microsoft.AspNetCore.Components.WebAssembly.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\Sebastian\source\repos\FeDam\FeDam\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\Sebastian\source\repos\FeDam\FeDam\_Imports.razor"
using FeDam;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\Sebastian\source\repos\FeDam\FeDam\_Imports.razor"
using FeDam.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 1 "C:\Users\Sebastian\source\repos\FeDam\FeDam\Pages\Interview.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\Sebastian\source\repos\FeDam\FeDam\Pages\Interview.razor"
using Newtonsoft.Json;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/counter")]
    public partial class Interview : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
        }
        #pragma warning restore 1998
#nullable restore
#line 38 "C:\Users\Sebastian\source\repos\FeDam\FeDam\Pages\Interview.razor"
       
    string currentDepartment { get; set; }
    string currentTitle { get; set; }
    int currentPositions { get; set; }
    private static InterviewModel interviewModel = new InterviewModel();
    InterviewModel[] interviewModels;
    private Guid idInterview { get; set; }

    public async Task Put()
    {
        Fill();
        if (idInterview == null)
            throw new Exception("Va rugam introduceti un id");
        var temp = await Http.PutAsJsonAsync("/api/Interviews/" + idInterview, interviewModel);
    }

    public async Task Delete()
    {
        if (!await JSRuntime.InvokeAsync<bool>("confirm", $"Are you sure you want to delete this interview?"))
            return;
        var temp = await Http.DeleteAsync("api/candidates/" + idInterview);
        var response = await temp.Content.ReadAsStringAsync();
    }

    public async Task PostInterview()
    {

        Fill();
        var temp = await Http.PostAsJsonAsync<InterviewModel>("api/Interviews", interviewModel);
        var response = await temp.Content.ReadAsStringAsync();

        //interviewModels = await Http.GetFromJsonAsync<InterviewModel[]>("Candidates");
        Console.WriteLine(response);
    }
    public void Fill()
    {
        interviewModel.Title = currentTitle;
        interviewModel.Department = currentDepartment;
        interviewModel.NumberOfPositions = currentPositions;
    }

    public class InterviewModel
    {
        public string Title { get; set; }

        public string Department { get; set; }

        public int NumberOfPositions { get; set; }
    }

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IJSRuntime JSRuntime { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private HttpClient Http { get; set; }
    }
}
#pragma warning restore 1591
