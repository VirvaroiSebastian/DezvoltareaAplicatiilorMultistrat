#pragma checksum "C:\Users\Sebastian\source\repos\FeDam\FeDam\Pages\CandidatiGet.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "b4a8df3e0b80ee40bec55c62361846ac027b0831"
// <auto-generated/>
#pragma warning disable 1591
namespace FeDam.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 2 "C:\Users\Sebastian\source\repos\FeDam\FeDam\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\Sebastian\source\repos\FeDam\FeDam\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\Sebastian\source\repos\FeDam\FeDam\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\Sebastian\source\repos\FeDam\FeDam\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\Sebastian\source\repos\FeDam\FeDam\_Imports.razor"
using Microsoft.AspNetCore.Components.WebAssembly.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\Sebastian\source\repos\FeDam\FeDam\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\Sebastian\source\repos\FeDam\FeDam\_Imports.razor"
using FeDam;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\Sebastian\source\repos\FeDam\FeDam\_Imports.razor"
using FeDam.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 1 "C:\Users\Sebastian\source\repos\FeDam\FeDam\Pages\CandidatiGet.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\Sebastian\source\repos\FeDam\FeDam\Pages\CandidatiGet.razor"
using Newtonsoft.Json;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/candidati")]
    public partial class CandidatiGet : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.AddMarkupContent(0, "<h3 class=\"header\">Candidati</h3>\r\n");
            __builder.AddMarkupContent(1, "<label for=\"interviews\">Choose an interview:</label>\r\n\r\n");
            __builder.OpenElement(2, "select");
            __builder.AddAttribute(3, "name", "interviews");
            __builder.AddAttribute(4, "id", "interview");
            __builder.OpenElement(5, "option");
            __builder.AddAttribute(6, "value", "Tester");
            __builder.AddContent(7, "Automation testing");
            __builder.CloseElement();
            __builder.AddMarkupContent(8, "\r\n    ");
            __builder.OpenElement(9, "option");
            __builder.AddAttribute(10, "value", "BE");
            __builder.AddContent(11, "Backend developer");
            __builder.CloseElement();
            __builder.AddMarkupContent(12, "\r\n    ");
            __builder.OpenElement(13, "option");
            __builder.AddAttribute(14, "value", "FE");
            __builder.AddContent(15, "Frontend developer");
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.AddMarkupContent(16, "\r\n<br>\r\n");
            __builder.OpenElement(17, "button");
            __builder.AddAttribute(18, "id", "1");
            __builder.AddAttribute(19, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 18 "C:\Users\Sebastian\source\repos\FeDam\FeDam\Pages\CandidatiGet.razor"
                         GetCandidates

#line default
#line hidden
#nullable disable
            ));
            __builder.AddContent(20, "Load data");
            __builder.CloseElement();
            __builder.AddMarkupContent(21, "\r\n\r\n");
            __builder.AddMarkupContent(22, "<style>\r\n    .header {\r\n        text-align: center;\r\n        font-size: 35px;\r\n        border: medium;\r\n        font-style: oblique;\r\n    }\r\n</style>");
#nullable restore
#line 29 "C:\Users\Sebastian\source\repos\FeDam\FeDam\Pages\CandidatiGet.razor"
 if (candidates == null)
{

#line default
#line hidden
#nullable disable
            __builder.AddMarkupContent(23, "<p><em>Loading...</em></p>");
#nullable restore
#line 32 "C:\Users\Sebastian\source\repos\FeDam\FeDam\Pages\CandidatiGet.razor"
}
else
{

#line default
#line hidden
#nullable disable
            __builder.OpenElement(24, "table");
            __builder.AddAttribute(25, "class", "table");
            __builder.AddMarkupContent(26, "<thead><tr><th>Name</th>\r\n                <th>Surname</th>\r\n                <th>BirthDay</th>\r\n                <th>CNP</th>\r\n                <th>PhoneNumber</th>\r\n                <th>Email</th></tr></thead>\r\n        ");
            __builder.OpenElement(27, "tbody");
#nullable restore
#line 47 "C:\Users\Sebastian\source\repos\FeDam\FeDam\Pages\CandidatiGet.razor"
             foreach (var candidate in candidates)
            {

#line default
#line hidden
#nullable disable
            __builder.OpenElement(28, "tr");
            __builder.OpenElement(29, "td");
            __builder.AddContent(30, 
#nullable restore
#line 50 "C:\Users\Sebastian\source\repos\FeDam\FeDam\Pages\CandidatiGet.razor"
                         candidate.Name

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(31, "\r\n                    ");
            __builder.OpenElement(32, "td");
            __builder.AddContent(33, 
#nullable restore
#line 51 "C:\Users\Sebastian\source\repos\FeDam\FeDam\Pages\CandidatiGet.razor"
                         candidate.Surname

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(34, "\r\n                    ");
            __builder.OpenElement(35, "td");
            __builder.AddContent(36, 
#nullable restore
#line 52 "C:\Users\Sebastian\source\repos\FeDam\FeDam\Pages\CandidatiGet.razor"
                         candidate.BirthDay

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(37, "\r\n                    ");
            __builder.OpenElement(38, "td");
            __builder.AddContent(39, 
#nullable restore
#line 53 "C:\Users\Sebastian\source\repos\FeDam\FeDam\Pages\CandidatiGet.razor"
                         candidate.CNP

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(40, "\r\n                    ");
            __builder.OpenElement(41, "td");
            __builder.AddContent(42, 
#nullable restore
#line 54 "C:\Users\Sebastian\source\repos\FeDam\FeDam\Pages\CandidatiGet.razor"
                         candidate.PhoneNumber

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(43, "\r\n                    ");
            __builder.OpenElement(44, "td");
            __builder.AddContent(45, 
#nullable restore
#line 55 "C:\Users\Sebastian\source\repos\FeDam\FeDam\Pages\CandidatiGet.razor"
                         candidate.Email

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.CloseElement();
#nullable restore
#line 57 "C:\Users\Sebastian\source\repos\FeDam\FeDam\Pages\CandidatiGet.razor"
            }

#line default
#line hidden
#nullable disable
            __builder.CloseElement();
            __builder.CloseElement();
#nullable restore
#line 60 "C:\Users\Sebastian\source\repos\FeDam\FeDam\Pages\CandidatiGet.razor"
}

#line default
#line hidden
#nullable disable
        }
        #pragma warning restore 1998
#nullable restore
#line 63 "C:\Users\Sebastian\source\repos\FeDam\FeDam\Pages\CandidatiGet.razor"
       

    private string CurrentTester { get; set; }
    private string CurrentBe { get; set; }
    private string CurrentFe { get; set; }
    private List<CandidateModel> candidates;
    private CandidateModel candidatiModel = new CandidateModel();

    public async Task GetCandidates()
    {
        string interviewID = "5fcbf4d7-3261-40e0-a60c-08d8b3d3009b";
        candidates = (await httpClient.GetFromJsonAsync<ApiResponseModel<List<CandidateModel>>>("api/interviews/id/candidates?id=" + interviewID)).Result;
    }
    public class ApiResponseModel<T>
    {
        public int Code { get; set; }
        public T Result { get; set; }
    }
     
    public class CandidateModel
    {
        public string InterviewId { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string BirthDay { get; set; }

        public string CNP { get; set; }

        public string PhoneNumber { get; set; }

        public string Email { get; set; }
    }

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private HttpClient httpClient { get; set; }
    }
}
#pragma warning restore 1591
